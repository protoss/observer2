package observer2.teleportation.tk.observer2;

import android.annotation.TargetApi;
import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by sha on 10/10/14.
 */
public class DataSampleAdapter {
    private ArrayList<DataSample> mDataArray;

    public DataSampleAdapter(){
        mDataArray = new ArrayList<DataSample>();
    }

    public boolean isEmpty(){
        return mDataArray.isEmpty();
    }

    public void addData(DataSample data){
        mDataArray.add(data);
    }

    public void clearData(){
        mDataArray.clear();
    }

    public int countData(){
        return mDataArray.size();
    }

    public int countData(int minor){
        int i=0;
        for (DataSample data :mDataArray){
                i++;
        }
        return i;
    }

    public ArrayList<DataSample> getDataArray(){
        return mDataArray;
    }

    @TargetApi(19)
    public boolean saveData(String name){
        if (isExternalStorageWritable()){

            File dir = new File(Environment.getExternalStoragePublicDirectory(
                    Environment.DIRECTORY_DOCUMENTS), "RadiationTracker");
            if (!dir.exists()){
                if (!dir.mkdirs()) {
                    Log.e("", "Directory not created");
                    return false;
                }
            }



            File file = new File(dir, name);


            try {
                OutputStream os = new FileOutputStream(file,true); // true => append
                String temp = "";

                // generate the content
                for(DataSample data: mDataArray){
                    temp += data.toCSVString();
                    temp += "\n";
                }

                os.write(temp.getBytes());
                os.close();
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }


            return true;
        }else{
            Log.d("","@string/storage_not_writable");
            return false;
        }
    }

    public String saveData(){
        String name = "RT_";
        Date date = new Date();
        SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd_kkmmss");
        String suffix = df.format(date);
        name += suffix;

        if(saveData(name))
            return name;
        else
            return null;
    }

    /* Checks if external storage is available for read and write */
    public boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            return true;
        }
        return false;
    }

    /* Checks if external storage is available to at least read */
    public boolean isExternalStorageReadable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state) ||
                Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
            return true;
        }
        return false;
    }


}
