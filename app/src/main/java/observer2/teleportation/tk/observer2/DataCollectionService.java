package observer2.teleportation.tk.observer2;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.Binder;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.Toast;

import com.cc.signalinfo.enums.NetworkType;
import com.cc.signalinfo.enums.Signal;
import com.cc.signalinfo.listeners.SignalListener;
import com.cc.signalinfo.signals.ISignal;
import com.cc.signalinfo.util.SignalArrayWrapper;
import com.cc.signalinfo.util.SignalMapWrapper;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import observer2.teleportation.tk.bs.location.CellIDInfo;
import observer2.teleportation.tk.bs.location.CellIDLocation;

public class DataCollectionService extends Service implements
        SignalListener.UpdateSignal,
        GooglePlayServicesClient.ConnectionCallbacks,
        GooglePlayServicesClient.OnConnectionFailedListener,
        LocationListener {
    private static final String TAG = DataCollectionService.class.getSimpleName();
    private LocalBinder mBinder = new LocalBinder();

    private final int TICK_WHAT = 0;
    private final int STOP_WHAT = 1;

    public boolean mIsStarted = false;

    private SignalListener mListener;

    public static final String NOTIFICATION = "tk.teleportation.observer2.service.receiver";
    public static final String NOTIFICATION_NEWDATA = "tk.teleportation.observer2.service.receiver.newdata";


    public static final String LOGNAME = "RT_Log";

    private String mDataString;
    private int mRSSI;

    private DataSampleAdapter mDataSampleAdapter;

    public boolean mIsRunning = false;
    private String[] mFilteredSignals = null;
    private TelephonyManager mTM;

    // do the job periodically 10 second: sample taking
    private final long mFrequency = 10000;


    // Location update related

    // Milliseconds per second
    private static final int MILLISECONDS_PER_SECOND = 1000;
    // Update frequency in seconds
    public static final int UPDATE_INTERVAL_IN_SECONDS = 60;  // update location every 60 sec
    // Update frequency in milliseconds
    private static final long UPDATE_INTERVAL =
            MILLISECONDS_PER_SECOND * UPDATE_INTERVAL_IN_SECONDS;
    // The fastest update frequency, in seconds
    private static final int FASTEST_INTERVAL_IN_SECONDS = 1;
    // A fast frequency ceiling in milliseconds
    private static final long FASTEST_INTERVAL =
            MILLISECONDS_PER_SECOND * FASTEST_INTERVAL_IN_SECONDS;

    // Define an object that holds accuracy and frequency parameters
    LocationRequest mLocationRequest;

    private LocationClient mLocationClient;
    private Location mCurrentLocation;


    public class LocalBinder extends Binder {
        DataCollectionService getService() {
            return DataCollectionService.this;
        }
    }

    private Handler mHandler = new Handler() {
        boolean stop = false;

        public void handleMessage(Message m) {
            if (stop)
                return;
            switch (m.what) {
                case TICK_WHAT:
                    doJob();
                    sendMessageDelayed(Message.obtain(this, TICK_WHAT), mFrequency);
                    break;
                case STOP_WHAT:
                    stop = true;
                    break;
            }
        }
    };

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "DataCollectionService Created!");
        mDataString = "Hello shit!";
        mDataSampleAdapter = new DataSampleAdapter();

        mListener = new SignalListener(this);
        mTM = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);

        mLocationClient = new LocationClient(this, this, this);

        mLocationRequest = LocationRequest.create();
        // Use high accuracy
        mLocationRequest.setPriority(
                LocationRequest.PRIORITY_HIGH_ACCURACY);
        // Set the update interval to 60 seconds
        mLocationRequest.setInterval(UPDATE_INTERVAL);
        // Set the fastest update interval to 1 second
        mLocationRequest.setFastestInterval(FASTEST_INTERVAL);


    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mIsRunning = false;
        mHandler.sendMessageDelayed(Message.obtain(mHandler, STOP_WHAT), mFrequency);
        // disconnect location update service
        mLocationClient.disconnect();
        // unregister PhoneStateListener
        mTM.listen(mListener, PhoneStateListener.LISTEN_NONE);

        Log.d(TAG, "DataCollectionService destroyed!");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        mLocationClient.connect();
        mTM.listen(mListener, PhoneStateListener.LISTEN_SIGNAL_STRENGTHS);

        doJobPeriodically();
        Log.d(TAG, "DataCollectionService started!");
        mIsRunning = true;
        return START_STICKY;
    }


    public void doJobPeriodically() {
        mIsStarted = true;
        mHandler.sendMessageDelayed(Message.obtain(mHandler, TICK_WHAT), mFrequency);
    }

    /*
    * The job that's been done periodically is implemented here
    * */
    private void doJob() {
        Intent intent = new Intent(NOTIFICATION);
        sendBroadcast(intent);
        Log.d(TAG, "do job once...");
        //Log.d(TAG, mDataString);


        // update location
        if (mLocationClient.isConnected()) {
            mCurrentLocation = mLocationClient.getLastLocation();

            if (mCurrentLocation!=null){
            String locationString = String.format("Current Loc: Lat %3f.6 Lon %3f.6",
                    mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude());

            Log.d(TAG, locationString);
            }
        }

        // set sample data
        setCellInfo();

    }

    public String getDataString(){
        return mDataString;
    }

    private void setCellInfo() {
        try {
            String infoText;
            ArrayList<CellIDInfo> cellList = CellIDLocation.getCellIDInfo(this);
            for (CellIDInfo cellInfo : cellList) {

                int rssi = cellInfo.getRssi();
                if (rssi == 0){
                    rssi = mRSSI;
                }

                infoText = String.format("%s | MCC: %s, MNC: %s, LAC:%d, CID: %d, RSSI: %d dBm\n",
                        cellInfo.getRadioType(),
                        cellInfo.getMobileCountryCode(),
                        cellInfo.getMobileNetworkCode(),
                        cellInfo.getLocationAreaCode(),
                        cellInfo.getCellId(),
                        rssi);


                Log.d(TAG, infoText);

                DataSample sample = new DataSample();
                sample.setRadioType("Cellular");
                sample.setTimeStamp(new Date());
                sample.setLatitude(mCurrentLocation.getLatitude());
                sample.setLongitude(mCurrentLocation.getLongitude());
                sample.setMCC(Integer.parseInt(cellInfo.getMobileCountryCode()));
                sample.setMNC(Integer.parseInt(cellInfo.getMobileNetworkCode()));
                sample.setLAC(cellInfo.getLocationAreaCode());
                sample.setCellID(cellInfo.getCellId());
                sample.setRSSI(rssi);
                sample.setNetworkType(cellInfo.getRadioType());

                mDataSampleAdapter.addData(sample);

            }


        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        mDataSampleAdapter.saveData(LOGNAME);
        Log.d(TAG, "Data log saved.");
        Intent i = new Intent(NOTIFICATION_NEWDATA);
        i.putExtra("data",mDataSampleAdapter.getDataArray().get(0));
        sendBroadcast(i);
        mDataSampleAdapter.clearData();

    }

    /*
    *  Implement the SignalListener.UpdateSignal
    *
    * */
    @Override
    public void setData(SignalArrayWrapper signalStrength) {
        if (signalStrength == null) {
            return;
        }
        mFilteredSignals = signalStrength.getFilteredArray();

        SignalMapWrapper signalMapWrapper = new SignalMapWrapper(mFilteredSignals, mTM);

        if (signalMapWrapper.hasData()) {
            Map<NetworkType, ISignal> networkTypes = signalMapWrapper.getNetworkMap();

            StringBuffer buff = new StringBuffer();

            for (Map.Entry<NetworkType, ISignal> data : networkTypes.entrySet()) {
                ISignal signal = data.getValue();
                NetworkType networkType = data.getKey();

                Log.d(TAG, "Signal updated...");
                buff.append(networkType.toString());
                buff.append(": ");
                buff.append(signal.getSignals().toString());
                buff.append("\n");

                if (networkType.toString() == "GSM"){
                    //Log.d(TAG, signal.getSignals().get(Signal.GSM_RSSI));
                    mRSSI = Integer.parseInt(signal.getSignals().get(Signal.GSM_RSSI));
                }
            }

            mDataString = buff.toString();

        }
    }

    /*
    * Call back to handle location update (implementation of LocationListener)
    * */

    @Override
    public void onLocationChanged(Location location) {
        // Report to the UI that the location was updated
        String msg = "Updated Location: " +
                Double.toString(location.getLatitude()) + "," +
                Double.toString(location.getLongitude());
        //Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
        Log.d(TAG, msg);
    }


    /*
    * Call back implementation for google play service
    * */

    @Override
    public void onConnected(Bundle bundle) {
        Toast.makeText(getApplicationContext(), "" +
                "Google Play Connected", Toast.LENGTH_SHORT).show();
        mCurrentLocation = mLocationClient.getLastLocation();
        mLocationClient.requestLocationUpdates(mLocationRequest, this);

    }

    @Override
    public void onDisconnected() {
        Toast.makeText(getApplicationContext(), "" +
                "Google Play disconnected", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Toast.makeText(getApplicationContext(), "" +
                "Google Play connection failed!", Toast.LENGTH_SHORT).show();
    }
}
