package observer2.teleportation.tk.observer2;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Date;

/**
 * Created by sha on 10/10/14.
 */
public class DataSample implements Parcelable {
    // Sampling time
    Date mTimeStamp;

    // Sampling location
    double mLatitude;
    double mLongitude;


    String mRadioType; // "Cellular", "Wifi"

    // Signal Strength in dBm
    int mRSSI;

    /*
    * Signal ID related parameters
    */

    // Cellular related parameters
    int mMCC;
    int mMNC;
    int mLAC;
    int mCellID;
    String mNetworkType;

    // Wifi related parameters
    String mSSID;
    String mMAC;

    public String toCSVString(){
        StringBuffer buff = new StringBuffer();
        buff.append(mTimeStamp.getTime() +"," + mLatitude +"," + mLongitude +"," + mRadioType+"," + mRSSI+",");
        buff.append(mMCC+",").append(mMNC+",").append(mLAC+",").append(mCellID+",").append(mNetworkType+",");
        buff.append(mSSID +"," +mMAC);
        return buff.toString();
    }

    public DataSample(){}

    public DataSample(Date timeStamp, double latitude, double longitude, String radioType, int RSSI, int MCC, int MNC, int LAC, int cellID, String networkType, String SSID, String MAC) {
        mTimeStamp = timeStamp;
        mLatitude = latitude;
        mLongitude = longitude;
        mRadioType = radioType;
        mRSSI = RSSI;
        mMCC = MCC;
        mMNC = MNC;
        mLAC = LAC;
        mCellID = cellID;
        mNetworkType = networkType;
        mSSID = SSID;
        mMAC = MAC;
    }

    public String getMAC() {
        return mMAC;
    }

    public void setMAC(String MAC) {
        mMAC = MAC;
    }

    public Date getTimeStamp() {
        return mTimeStamp;
    }

    public void setTimeStamp(Date timeStamp) {
        mTimeStamp = timeStamp;
    }

    public double getLatitude() {
        return mLatitude;
    }

    public void setLatitude(double latitude) {
        mLatitude = latitude;
    }

    public double getLongitude() {
        return mLongitude;
    }

    public void setLongitude(double longitude) {
        mLongitude = longitude;
    }

    public String getRadioType() {
        return mRadioType;
    }

    public void setRadioType(String radioType) {
        mRadioType = radioType;
    }

    public int getRSSI() {
        return mRSSI;
    }

    public void setRSSI(int RSSI) {
        mRSSI = RSSI;
    }

    public int getMCC() {
        return mMCC;
    }

    public void setMCC(int MCC) {
        mMCC = MCC;
    }

    public int getMNC() {
        return mMNC;
    }

    public void setMNC(int MNC) {
        mMNC = MNC;
    }

    public int getLAC() {
        return mLAC;
    }

    public void setLAC(int LAC) {
        mLAC = LAC;
    }

    public int getCellID() {
        return mCellID;
    }

    public void setCellID(int cellID) {
        mCellID = cellID;
    }

    public String getNetworkType() {
        return mNetworkType;
    }

    public void setNetworkType(String networkType) {
        mNetworkType = networkType;
    }

    public String getSSID() {
        return mSSID;
    }

    public void setSSID(String SSID) {
        mSSID = SSID;
    }



    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeStringArray(new String[]{"" + this.mTimeStamp.getTime(),
                "" + this.getLatitude(),
                "" + this.mLongitude,
                "" + this.mRSSI
        });

    }


/** Static field used to regenerate object, individually or as arrays */
    public static final Parcelable.Creator<DataSample> CREATOR = new Parcelable.Creator<DataSample>() {
        public DataSample createFromParcel(Parcel pc) {
            return new DataSample(pc);
        }
        public DataSample[] newArray(int size) {
            return new DataSample[size];
        }
    };

    /**Ctor from Parcel, reads back fields IN THE ORDER they were written */

    public DataSample(Parcel pc){
        String[] data = new String[4];

        pc.readStringArray(data);
        this.mTimeStamp = new Date(Long.parseLong(data[0]));
        this.mLatitude = Double.parseDouble(data[1]);
        this.mLongitude = Double.parseDouble(data[2]);
        this.mRSSI = Integer.parseInt(data[3]);
    }


}

