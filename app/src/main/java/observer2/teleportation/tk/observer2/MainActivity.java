package observer2.teleportation.tk.observer2;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.res.AssetManager;
import android.os.Bundle;
import android.os.Environment;
import android.os.IBinder;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.google.android.gms.maps.GoogleMap.*;


public class MainActivity extends Activity {
    DataCollectionService mDataCollectionService;
    Button mButtonStart;
    Button mButtonMap;


    private static final String TAG = MainActivity.class.getSimpleName();

    private BroadcastReceiver receiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle bundle = intent.getExtras();
//            if (isMyServiceRunning(DataCollectionService.class)) {
//                Toast.makeText(getBaseContext(), mDataCollectionService.getDataString(), Toast.LENGTH_SHORT).show();
//            }
        }

    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mButtonStart = (Button)findViewById(R.id.bt_start);
        mButtonStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent= new Intent(MainActivity.this, DataCollectionService.class);

                if (mDataCollectionService.mIsRunning){
                    stopService(intent);
                    unbindService(mConnection);
                    mButtonStart.setText("Start Service");

                } else {
                    startService(intent);
                    bindService(intent,mConnection,Context.BIND_AUTO_CREATE);
                    mButtonStart.setText("Stop Service");

                }

            }
        });

        mButtonMap = (Button)findViewById(R.id.bt_map);
        mButtonMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, MapActivity.class);
                startActivity(intent);
            }
        });


    }


    @Override
    protected void onResume() {
        super.onResume();

        Intent intent= new Intent(MainActivity.this, DataCollectionService.class);
        bindService(intent,mConnection,Context.BIND_AUTO_CREATE);
        registerReceiver(receiver, new IntentFilter(DataCollectionService.NOTIFICATION));

    }

    @Override
    protected void onPause() {
        super.onPause();

        //TODO: do we need to unbind services here?
//        unbindService(mConnection);

        unregisterReceiver(receiver);
    }

    private ServiceConnection mConnection = new ServiceConnection() {

        public void onServiceConnected(ComponentName className,
                                       IBinder binder) {
            DataCollectionService.LocalBinder b = (DataCollectionService.LocalBinder) binder;
            mDataCollectionService = b.getService();
            Toast.makeText(MainActivity.this, "Connected", Toast.LENGTH_SHORT)
                    .show();
            Log.d(TAG, "Service connected.");

            if (mDataCollectionService.mIsRunning){
                mButtonStart.setText("Stop Service");
            } else {
                mButtonStart.setText("Start Service");
            }

        }


        public void onServiceDisconnected(ComponentName className) {
            mDataCollectionService = null;
            Log.d(TAG, "Service disconnected.");
        }
    };

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }


}
