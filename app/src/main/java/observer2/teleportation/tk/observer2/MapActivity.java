package observer2.teleportation.tk.observer2;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Environment;
import android.os.Parcelable;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class MapActivity extends Activity {
    private GoogleMap map;

    /*
    * Receiver for background service messages
    * */
    private BroadcastReceiver receiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle bundle = intent.getExtras();
            DataSample data = (DataSample) bundle.getParcelable("data");
            Toast.makeText(MapActivity.this, "Updated RSSI: "+data.getRSSI()+" dBm",Toast.LENGTH_SHORT).show();
//            if (isMyServiceRunning(DataCollectionService.class)) {
//                Toast.makeText(getBaseContext(), mDataCollectionService.getDataString(), Toast.LENGTH_SHORT).show();
//            }
            LatLng loc = new LatLng(data.getLatitude(),data.getLongitude());

            SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd-kkmmss");
            String timeString = df.format(data.getTimeStamp());


            Marker marker = map.addMarker(new MarkerOptions().position(loc).alpha(0.2f).title(timeString)
                    .snippet("RSSI: " + data.getRSSI() + " dBm"));

            marker.showInfoWindow();

        }




    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);

        map = ((MapFragment) getFragmentManager().findFragmentById(R.id.map)) .getMap();
        map.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener(){
            @Override
            public void onInfoWindowClick(Marker marker) {
                marker.showInfoWindow();
            }
        });

        showData(map);

    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(receiver, new IntentFilter(DataCollectionService.NOTIFICATION_NEWDATA));

    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(receiver);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.map, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /*
    *  Load data from CSV file
    * */

    private List<List<String>> loadData(){
        File dir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_DOCUMENTS), "RadiationTracker");

        File file = new File(dir, DataCollectionService.LOGNAME);

        if(!file.exists())
            return null;

        List<List<String>> CSVData = null;
        try {
            CSVData = readTXTFile(file.getAbsolutePath());
        } catch (IOException e) {
            e.printStackTrace();
        }


//        CSVFile csvFile = new CSVFile(csvStream);
//        List result = csvFile.read();
//        return result;
        return CSVData;

    }

    /*
    *  Show data on the map
    * */

    private void showData(GoogleMap map){
        List<List<String>> data = loadData();
        if (data == null || data.size() == 0)
            return;

        LatLng loc = null;
        Marker marker = null;
        for(List<String> row :data){
            Date time = new Date(Long.parseLong(row.get(0)));
            SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd-kkmmss");
            String timeString = df.format(time);
            loc = new LatLng(Double.parseDouble(row.get(1)),Double.parseDouble(row.get(2)));
            marker = map.addMarker(new MarkerOptions().position(loc).alpha(0.2f).title(timeString)
                    .snippet("RSSI: " + row.get(4) + " dBm"));
        }

        // Move the camera instantly to hamburg with a zoom of 15.
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(loc, 15));
        // Zoom in, animating the camera.
        map.animateCamera(CameraUpdateFactory.zoomTo(10), 2000, null);

        marker.showInfoWindow();

    }

    /*
    * Utility function to read CSV file
    * */
    private static List<List<String>> readTXTFile(String csvFileName) throws IOException {

        String line = null;
        BufferedReader stream = null;
        List<List<String>> csvData = new ArrayList<List<String>>();

        try {
            stream = new BufferedReader(new FileReader(csvFileName));
            while ((line = stream.readLine()) != null) {
                String[] splitted = line.split(",");
                List<String> dataLine = new ArrayList<String>(splitted.length);
                for (String data : splitted)
                    dataLine.add(data);
                csvData.add(dataLine);
            }
        } finally {
            if (stream != null)
                stream.close();
        }

        return csvData;

    }
}
