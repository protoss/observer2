package observer2.teleportation.tk.bs.location;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.location.Location;
import android.telephony.NeighboringCellInfo;
import android.telephony.TelephonyManager;
import android.telephony.cdma.CdmaCellLocation;
import android.telephony.gsm.GsmCellLocation;
import android.util.Log;

/**
 * @author jiangzm
 */
public class CellIDLocation {

    private static final String TAG = CellIDLocation.class.getSimpleName();

    public static ArrayList<CellIDInfo> getCellIDInfo(Context context) throws Exception {
        TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);

        ArrayList<CellIDInfo> CellID = new ArrayList<CellIDInfo>();
        Location loc = null;
        int type = tm.getNetworkType();
        Log.d(TAG, "getCellIDInfo-->         NetworkType = " + type);
        int phoneType = tm.getPhoneType();
        Log.d(TAG, "getCellIDInfo-->         phoneType = " + phoneType);

        // �й����ΪCTC
        // NETWORK_TYPE_EVDO_A���й����3G��getNetworkType
        // NETWORK_TYPE_CDMA����2G��CDMA
        if (type == TelephonyManager.NETWORK_TYPE_CDMA || type == TelephonyManager.NETWORK_TYPE_1xRTT
                || type == TelephonyManager.NETWORK_TYPE_EVDO_0 || type == TelephonyManager.NETWORK_TYPE_EVDO_A) {
            CdmaCellLocation location = (CdmaCellLocation) tm.getCellLocation();
            int cellIDs = location.getBaseStationId();
            int networkID = location.getNetworkId();
            StringBuilder nsb = new StringBuilder();
            nsb.append(location.getSystemId());

            // TODO: how to check the RSSI for current cell?

            CellIDInfo info = new CellIDInfo(cellIDs, networkID, tm.getNetworkOperator().substring(0, 3),
                    nsb.toString(), "cdma", 0);
            CellID.add(info);

            // �ٽ��վ��Ϣ
            List<NeighboringCellInfo> list = tm.getNeighboringCellInfo();

            int size = list.size();
            for (int i = 0; i < size; i++) {
                CellIDInfo infoneighbours = new CellIDInfo(list.get(i).getCid(), networkID,
                        tm.getNetworkOperator().substring(0, 3), nsb.toString(), "cdma", list.get(i).getRssi());
                CellID.add(infoneighbours);
            }

        }
        // �ƶ�2G�� + CMCC + 2
        // type = NETWORK_TYPE_EDGE
//        else if (type == TelephonyManager.NETWORK_TYPE_EDGE) {
//            GsmCellLocation location = (GsmCellLocation) tm.getCellLocation();
//            int cellIDs = location.getCid();
//            int lac = location.getLac();
//            CellIDInfo info = new CellIDInfo(cellIDs, lac, tm.getNetworkOperator().substring(3, 5),
//                    tm.getNetworkOperator().substring(0, 3), "gsm", 0);
//            CellID.add(info);
//            // �ٽ��վ��Ϣ
//            List<NeighboringCellInfo> list = tm.getNeighboringCellInfo();
//
//            int size = list.size();
//            for (int i = 0; i < size; i++) {
//                CellIDInfo infoneighbours = new CellIDInfo(list.get(i).getCid(), lac, tm.getNetworkOperator().substring(3, 5),
//                        tm.getNetworkOperator().substring(0, 3), "gsm", list.get(i).getRssi());
//                CellID.add(infoneighbours);
//            }
//        }
        // ��ͨ��2G������� China Unicom 1 NETWORK_TYPE_GPRS
        else if (type == TelephonyManager.NETWORK_TYPE_GPRS) {
            GsmCellLocation location = (GsmCellLocation) tm.getCellLocation();
            int cellIDs = location.getCid();
            int lac = location.getLac();
            CellIDInfo info = new CellIDInfo(cellIDs, lac, "", "", "gsm", 0);
            // ������ԣ���ȡ��ͨ����������б���ȥ�����������ִ��󣬴�������ΪJSON Parsing Error
            // info.mobileNetworkCode = tm.getNetworkOperator().substring(3, 5);
            // info.mobileCountryCode = tm.getNetworkOperator().substring(0, 3);
            CellID.add(info);
            List<NeighboringCellInfo> list = tm.getNeighboringCellInfo();

            int size = list.size();
            for (int i = 0; i < size; i++) {
                CellIDInfo infoneighbours = new CellIDInfo(list.get(i).getCid(), lac, "", "", "gsm", list.get(i).getRssi());
                CellID.add(infoneighbours);
            }

        } else if (type == TelephonyManager.NETWORK_TYPE_UMTS || type == TelephonyManager.NETWORK_TYPE_HSPA || type == TelephonyManager.NETWORK_TYPE_EDGE) {
            GsmCellLocation location = (GsmCellLocation) tm.getCellLocation();
            int cellIDs = location.getCid();
            int lac = location.getLac();
            CellIDInfo info = new CellIDInfo(cellIDs, lac,  tm.getNetworkOperator().substring(0, 3),
                    tm.getNetworkOperator().substring(3, 5), "gsm", 0);
            CellID.add(info);

            Log.d(TAG,tm.getNetworkOperator());

            List<NeighboringCellInfo> list = tm.getNeighboringCellInfo();


            for (NeighboringCellInfo cell : list){
                CellIDInfo infoneighbours = new CellIDInfo(cell.getCid(), cell.getLac(),
                        tm.getNetworkOperator().substring(0, 3), tm.getNetworkOperator().substring(3, 5), "gsm", cell.getRssi());
                CellID.add(infoneighbours);

            }

//            int size = list.size();

//            for (int i = 0; i < size; i++) {
//                CellIDInfo infoneighbours = new CellIDInfo(list.get(i).getCid(), lac, tm.getNetworkOperator().substring(0, 3),
//                        tm.getNetworkOperator().substring(3, 5), "gsm", list.get(i).getRssi());
//                CellID.add(infoneighbours);
//            }

        } else {
            //throw new Exception("��֧�ֵ�ǰ�ƶ����綨λ");
            Log.d("RadiationTest", "��֧�ֵ�ǰ�ƶ����綨λ: networkType = " + type);

        }


        return CellID;
    }

}