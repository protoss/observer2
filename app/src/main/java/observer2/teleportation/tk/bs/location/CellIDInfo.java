package observer2.teleportation.tk.bs.location;

// source from https://github.com/jzsues/power_system_review_client/blob/master/src/com/zvidia/review/location/CellIDInfo.java
// - Sha 

public class CellIDInfo {
    private int cellId;
    private int locationAreaCode;

    private String mobileCountryCode;

    private String mobileNetworkCode;

    private String radioType;

    private int rssi;

    public CellIDInfo() {
        super();
    }

    public CellIDInfo(int cellId, int locationAreaCode, String mobileCountryCode, String mobileNetworkCode, String radioType, int rssi) {
        super();
        this.cellId = cellId;
        this.mobileCountryCode = mobileCountryCode;
        this.mobileNetworkCode = mobileNetworkCode;
        this.locationAreaCode = locationAreaCode;
        this.radioType = radioType;
        this.rssi = rssi;
    }

    public int getCellId() {
        return cellId;
    }

    public void setCellId(int cellId) {
        this.cellId = cellId;
    }

    public String getMobileCountryCode() {
        return mobileCountryCode;
    }

    public void setMobileCountryCode(String mobileCountryCode) {
        this.mobileCountryCode = mobileCountryCode;
    }

    public String getMobileNetworkCode() {
        return mobileNetworkCode;
    }

    public void setMobileNetworkCode(String mobileNetworkCode) {
        this.mobileNetworkCode = mobileNetworkCode;
    }

    public int getLocationAreaCode() {
        return locationAreaCode;
    }

    public void setLocationAreaCode(int locationAreaCode) {
        this.locationAreaCode = locationAreaCode;
    }

    public String getRadioType() {
        return radioType;
    }

    public void setRadioType(String radioType) {
        this.radioType = radioType;
    }

    public int getRssi() {
        return rssi;
    }

    public void setRssi(int rssi) {
        this.rssi = rssi;
    }
}